package main

import (
	"chit.com/pkg/routers"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"net/http"
	"os"
	"path/filepath"
)

type Server struct {
	Router *chi.Mux
	// Db, config can be added here
}

func CreateNewServer() *Server {
	s := &Server{}
	s.Router = chi.NewRouter()
	return s
}

func (s *Server) MountHandlers() {
	s.Router.Mount("/test", routers.SetupTestRouter())
	s.Router.Mount("/static", routers.SetupFrontendRouter())
	s.Router.Mount("/api", routers.SetupApiRouter())

	workDir, _ := os.Getwd()

	s.Router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		indexPath := filepath.Join(workDir, "/dist/frontend/index.html")
		http.ServeFile(w, r, indexPath)
	})

}

func (s *Server) RegisterMiddleware() {
	s.Router.Use(middleware.Logger)
	s.Router.Use(middleware.Heartbeat("/isAlive"))
}

func main() {
	s := CreateNewServer()
	s.RegisterMiddleware()
	s.MountHandlers()

	fmt.Println("Starting chit server on port 3000")
	http.ListenAndServe(":3000", s.Router)
}
