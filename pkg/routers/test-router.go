package routers

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	"net/http"
)

// HelloWorld api Handler
func HelloWorld(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello World!"))
}

func SetupTestRouter() *chi.Mux {
	r := chi.NewRouter()
	r.Get("/", HelloWorld)
	r.Get("/log-param/{param}", func(w http.ResponseWriter, r *http.Request) {
		param := chi.URLParam(r, "param")
		fmt.Printf("The parameter I got was: %v\n", param)
		w.Write([]byte(fmt.Sprintf("Thank you. The param you sent in was: %v", param)))
	})
	return r
}
