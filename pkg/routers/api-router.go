package routers

import (
	"context"
	"errors"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"io/ioutil"
	"net/http"
)

type ErrResponse struct {
	Err            error `json:"-"` // low-level runtime error
	HTTPStatusCode int   `json:"-"` // http response status code

	StatusText string `json:"status"`          // user-level status message
	AppCode    int64  `json:"code,omitempty"`  // application-specific error code
	ErrorText  string `json:"error,omitempty"` // application-level error message, for debugging
}

func (e *ErrResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	return nil
}

func ErrInvalidRequest(err error) render.Renderer {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: 400,
		StatusText:     "Invalid request.",
		ErrorText:      err.Error(),
	}
}

func ErrRender(err error) render.Renderer {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: 422,
		StatusText:     "Error rendering response.",
		ErrorText:      err.Error(),
	}
}

var ErrNotFound = &ErrResponse{HTTPStatusCode: 404, StatusText: "Resource not found."}

func (msg *Message) Bind(r *http.Request) error {
	if msg == nil {
		return errors.New("this ain't a Message")
	}
	return nil
}

func (msg *Message) Render(w http.ResponseWriter, r *http.Request) error {
	// Pre-processing before a response is marshalled and sent across the wire
	return nil
}

type Message struct {
	Text string `json:text`
}

func SetupMessagesRouter() *chi.Mux {
	messageRouter := chi.NewRouter()
	messageRouter.Post("/new", func(w http.ResponseWriter, r *http.Request) {
		data := &Message{}
		if err := render.Bind(r, data); err != nil {
			render.Render(w, r, ErrInvalidRequest(err))
		}
		text := data.Text
		fmt.Printf("got new message: %v", text)
		render.Render(w, r, &Message{Text: text})
	})
	return messageRouter
}

func MyMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// create new context from `r` request context, and assign key `"user"`
		// to value of `"123"`
		ctx := context.WithValue(r.Context(), "user", "123")

		// call the next handler in the chain, passing the response writer and
		// the updated request object with the new context value.
		//
		// note: context.Context values are nested, so any previously set
		// values will be accessible as well, and the new `"user"` key
		// will be accessible from this point forward.
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func SetupApiRouter() *chi.Mux {
	apiRouter := chi.NewRouter()
	apiRouter.Use(MyMiddleware)
	apiRouter.Get("/test-api", func(w http.ResponseWriter, r *http.Request) {
		user, ok := r.Context().Value("user").(string) // .(string) is a type assertion! at runtime, if of correct type then ok else err
		if ok == true {
			w.Write([]byte(fmt.Sprintf("Response from api router. User: %v", user)))

		} else {
			w.Write([]byte("Incorrect type of user."))
		}
	})
	apiRouter.Get("/favorite", func(w http.ResponseWriter, r *http.Request) {
		resp, err := http.Get("http://localhost:3001/favorite/")
		if err != nil {
			fmt.Printf("Get kall virket ikke. Error: %v", err)
		}
		defer resp.Body.Close()
		favorite, err := ioutil.ReadAll(resp.Body)

		if err != nil {
			fmt.Printf("Get kall virket, men det klikket under ioutil.ReadAll. Error: %v", err)
		}
		w.Write(favorite)
	})
	apiRouter.Mount("/messages", SetupMessagesRouter())
	return apiRouter
}
